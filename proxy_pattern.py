import abc

class AbstractClass(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def execute(self):
        pass

class Boss(AbstractClass):

    def execute(self):
        print("Budget approved")


class ProxyBoss(AbstractClass):

    def __init__(self, budget):
        self.is_approved = self.get_approve(budget)
        self.executor = Boss()

    def get_approve(self, budget):
        if int(budget) < 1000:
            return True
        else:
            return False

    def execute(self):
        if self.is_approved:
            self.executor.execute()
        else:
            print("Budget didn't approved")

proxyBoss = ProxyBoss(100)
proxyBoss.execute()

